class Shot
    def initialize(shot_object)
        @shot = shot_object.attributes
        parse_description
        parse_position
    end

    def shot_id
        @shot['id'].to_s.gsub('shot', '')
    end

    def text
        @shot['data-text'].to_s.delete('.')
    end

    def period
        ''' '''
        @shot['data-period'].to_s.to_i
    end

    def shooter_id
        @shot['data-shooter'].to_s.to_i
    end

    def outcome
        @shot['class'].to_s
    end

    def homeaway
        @shot['data-homeaway'].to_s
    end

    def style
        @shot['style'].to_s.split(';')
    end

    def return_hash
        { shot_id: shot_id, period: period, shooter_id: shooter_id, outcome: outcome, homeaway: homeaway, shooter_name: @shooter_name, shot_type: @shot_type, assisted_by: @assisted_by, left: @left, top: @top, position: [@left, @top] }
    end

    protected

    def parse_position
        # Jank City
        left = style.detect { |val| val.include? 'left' }
        top = style.detect { |val| val.include? 'top' }
        @left = left.to_s.gsub('left:', '').delete('%').to_i
        @top = top.to_s.gsub('top:', '').delete('%').to_i
    end

    def parse_description
        if text.include? 'Assisted by'
            parse_assist
        else
            parse_single_shot(text)
            @assisted_by = nil
        end
    end

    def parse_single_shot(s)
        single = s.split(" #{outcome} ")
        @shooter_name = single[0]
        @shot_type = single[1]
    end

    def parse_assist
        assist = text.split(' Assisted by ')
        parse_single_shot(assist[0])
        @assisted_by = assist[1]
    end
end
