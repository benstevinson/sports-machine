require 'optparse'
require './Game.rb'

gameID = nil

# parse arguments
file = __FILE__
ARGV.options do |opts|
  opts.on("-g", "--gameid=val", Integer)  { |val| gameID = val }
  opts.parse!
end

if gameID.nil?
  puts "Uh oh, this requires a Game ID!"
else
  game = Game.new(gameID)
  #puts game.shot_collection
  puts "Generating CSV"
  game.generate_csv
end
