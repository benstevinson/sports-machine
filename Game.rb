require 'nokogiri'
require 'open-uri'
require './shot.rb'
require 'csv'
require 'time'

class Game
    attr_reader :shot_collection
    def initialize(gameID)
        @gameID = gameID
        @game_url = "http://www.espn.com/mens-college-basketball/game?gameId=#{@gameID}"
        @shot_collection = []
        scrape_game
        generate_shots
    end

    def generate_shots
        @game.css('ul.away-team, ul.home-team').children.each do |sh|
            s = Shot.new(sh)
            @shot_collection << s.return_hash unless s.shot_id == ''
        end
    end

    def scrape_game
        @game = Nokogiri::HTML(open(@game_url))
    end

		def csv_title
			@game.css('title').children.to_s.gsub('-', '').gsub('  ', '-').gsub(' ', '-').gsub('.', '').gsub(',', '').downcase
		end

    def generate_csv
        CSV.open("#{@gameID}-#{csv_title}.csv", 'wb') do |csv|
            csv << @shot_collection.first.keys
            @shot_collection.each do |hash|
                csv << hash.values
            end
        end
  end
end
